# Clean Coded Application

Cucumber practicing with a clean application architecture

Read the cucumber features in `src/test/resources/` to get a clue what this app is doing.
In short:

  * Classes JSample.Base/Map/List encapsulates JSON-Library classes and APIs
  * Classes Request, Response, and Usecase uses JSample objects
  * usecases.Factory creates usecases.Base objects based on request.path/method
  * usecases.Base.execute returns a Response-Object of form { meta:{}, data: .... }
  
For demonstration there are two example-implementations of use cases

  * Ping `{ method: get, path: ping } => { data: pong, meta: {} }`
  * Echo `{ method: get, path: echo, body: hello } => { data: HELLO, meta: {} }`

## Get the code

Git:

    git clone https://bitbucket.org/n0ckenfell/jsample.git
    cd jsample

## Use Maven

### Run the tests

Open a command window and run:

    mvn test

This runs Cucumber features using Cucumber's JUnit runner. The `@RunWith(Cucumber.class)` annotation on the `RunCukesTest`
class tells JUnit to kick off Cucumber.

### Build the project

To build the artifacts (.jar) run:

    mvn clean package

### To start the local server run

    java -cp target/cc_application-0.0.1-jar-with-dependencies.jar cc_application.support.CCApplication
    
and open `http://localhost:8808` in your browser.
try the following commands

    http://localhost:8808/ping             # => PONG
    http://localhost:8808/echo/hello       # => HELLO
    http://localhost:8808/server/shutodwn  # => terminates the server

## Extend new use cases

  1. Define a class in cc_application.usecases, derived from Base
  2. Overwrite the method `run` and call at least `setData(".....")`
  3. rebuild and run the server with `./build_and_run`. (your new use-case should work now)
  
----

# Note

 - Use `Config.log(LEVEL,MESSAGE)` for logging
 - Use `Config.clock()` in all calls to `.now(Config.clock())` instead of `.now()`

----

# Cucumber Options

## Using IntelliJ Plugin

Open Edit Run configurations and setup the following values

    Main class: cucumber.api.cli.Mai
    Clue: cc_application
    Feature path: src/test/resources
    Program arguments:  --plugin org.jetbrains.plugins.cucumber.java.run.CucumberJvmSMFormatter --no-monochrome
    Use classpath of: cc_application


## Using Cucumber thru maven

### Overriding options

The Cucumber runtime parses command line options to know what features to run, where the glue code lives, what plugins to use etc.
When you use the JUnit runner, these options are generated from the `@CucumberOptions` annotation on your test.

Sometimes it can be useful to override these options without changing or recompiling the JUnit class. This can be done with the
`cucumber.options` system property. The general form is:

Using Maven:

    mvn -Dcucumber.options="..." test


Let's look at some things you can do with `cucumber.options`. Try this:

    -Dcucumber.options="--help"

That should list all the available options.

*IMPORTANT*

When you override options with `-Dcucumber.options`, you will completely override whatever options are hard-coded in
your `@CucumberOptions` or in the script calling `cucumber.api.cli.Main`. There is one exception to this rule, and that
is the `--plugin` option. This will not _override_, but _add_ a plugin. The reason for this is to make it easier
for 3rd party tools (such as [Cucumber Pro](https://cucumber.pro/)) to automatically configure additional plugins by appending arguments to a `cucumber.properties`
file.

#### Run a subset of Features or Scenarios

Specify a particular scenario by *line* (and use the pretty plugin, which prints the scenario back)

    -Dcucumber.options="classpath:cc_application/general_usecase.feature --plugin pretty"

This works because Maven puts `./src/test/resources` on your `classpath`.
You can also specify files to run by filesystem path:

    -Dcucumber.options="src/test/resources/cc_application/tests/usecases/general_usecase.feature --plugin pretty"

You can also specify what to run by *tag*:

    -Dcucumber.options="--tags @bar --plugin pretty"

#### Running only the scenarios that failed in the previous run

    -Dcucumber.options="@target/rerun.txt"

This works as long as you have the `rerun` formatter enabled.

#### Specify a different formatter:

For example a JUnit formatter:

    -Dcucumber.options="--plugin junit:target/cucumber-junit-report.xml"

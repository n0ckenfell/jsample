# CHANGELOG


## 2016-01-06

  * Added `Config.clock()`, and `Config.freezeTimeAt(DateTime)`
    and tests for use case `Time`
    

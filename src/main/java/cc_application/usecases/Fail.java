package cc_application.usecases;

import cc_application.exceptions.JsonParseException;

public class Fail extends Base {
    public Fail(cc_application.requests.Base request) throws JsonParseException {
        super(request);
    }

    @Override
    public void run() {
        addError(400, "Test friendly failed on request");
    }


}

package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;
import cc_application.routes.Route;

import java.lang.reflect.InvocationTargetException;

import static cc_application.routes.Route.buildRoute;

/**
 * Build concrete use cases based on `useCaseName()` of the request
 */
public class Factory {
    public static Base buildUseCase(cc_application.requests.Base request) throws JsonParseException, InvalidRequestException {
        Route route = null;
        try {
            route = buildRoute(request);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException e) {
            e.printStackTrace();
        }
        return route != null ? route.usecase() : null;
    }
}

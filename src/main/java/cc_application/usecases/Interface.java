package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.jsonapi.JSONAPIErrors;

import java.util.HashMap;

/**
 * A Use case must have an implementation of execute(), meta(), and data() methods.
 */
interface Interface {
    cc_application.responses.Base execute() throws InvalidRequestException;

    // JSON API Mandatory

    Object data();

    JSONAPIErrors errors();

    HashMap meta();


    // JSON API Optionals

    HashMap jsonApi();

    HashMap links();

    HashMap included();

}

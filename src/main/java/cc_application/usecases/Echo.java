package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;

import java.util.ArrayList;

/**
 * <h3>Usecase Echo</h3>
 *
 * <h4>Request</h4>
 * <pre>
 *     { "method" : "get", "path" : "echo", "body" : "Any String" }
 * </pre>
 *
 * <h4>Response</h4>
 * <pre>
 *     { "meta" : {}, "body" : "ANY STRING" }
 * </pre>
 */
public class Echo extends Base {
    public Echo(cc_application.requests.Base request) throws JsonParseException {
        super(request);
    }

    @Override
    public void run() throws InvalidRequestException {
        ArrayList<String> arguments = request.arguments();
        try {
            setData(arguments.get(1).toUpperCase());
        } catch (IndexOutOfBoundsException e) {
            setData(request.body().toUpperCase());
        }
    }

}

package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;
import cc_application.support.Config;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class Time extends Base {

    public Time(cc_application.requests.Base request) throws JsonParseException {
        super(request);
    }

    @Override
    public void run() {
        String reqTime = parseRequest();

        if(reqTime==null) return;

        ZonedDateTime requestedDate = getZonedDateTime(reqTime);
        if(requestedDate != null) {
            HashMap<String, Object> dmap = buildResponseHash(requestedDate);
            this.setData(dmap);
        }
    }

    private String parseRequest() {
        String reqTimeString = null;
        try {
            if(request.arguments().size() > 1)
                reqTimeString = request.arguments().get(1);
            else
                reqTimeString = OffsetDateTime.now(Config.clock()).toString();
        } catch (InvalidRequestException e) {
            this.addError(0, e.getMessage());
            e.printStackTrace();
        }
        return reqTimeString;
    }

    private HashMap<String, Object> buildResponseHash(ZonedDateTime requestedDate) {
        HashMap<String, Object> dmap = new HashMap<>();
        dmap.put("iso", requestedDate.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        dmap.put("year", requestedDate.getYear());
        dmap.put("month", requestedDate.getMonthValue());
        dmap.put("monthName", requestedDate.getMonth());
        dmap.put("dayOfMonth", requestedDate.getDayOfMonth());
        dmap.put("dayOfWeek", requestedDate.getDayOfWeek());
        dmap.put("hour", requestedDate.getHour());
        dmap.put("minute", requestedDate.getMinute());
        dmap.put("second", requestedDate.getSecond());
        dmap.put("offset", requestedDate.getOffset());
        dmap.put("nano", requestedDate.getNano());
        return dmap;
    }

    private ZonedDateTime getZonedDateTime(String reqDateTimeString) {
        ZonedDateTime requestedDate;
        try {
            requestedDate = ZonedDateTime.parse(reqDateTimeString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        } catch (Exception e) {
            addError(0, e.getMessage());
            requestedDate = null;
        }
        return requestedDate;
    }

}


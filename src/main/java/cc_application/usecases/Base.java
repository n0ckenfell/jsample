package cc_application.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;
import cc_application.jsonapi.JSONAPIErrors;

import java.util.HashMap;

/**
 * The Base Use case class executes the given request by initializing the main parts of
 * a JSON-API response. Which are:
 *   * data or errors
 *   * meta
 *   * and other parts, described in
 *   <a href="http://jsonapi.org/format/#document-structure">jsonapi document structure</a>
 *
 * To implement a concrete use case overwrite the following methods in  any inheritance:
 *   * buildData(), buildErrors(), buildMeta(), buildLinks(), ....
 * For more information read the jsonapi-document mentioned above.
 */
public abstract class Base implements Interface {
    cc_application.requests.Base request;
    private cc_application.jsample.Base jsonResponse;
    private Object data;
    private HashMap meta;
    private JSONAPIErrors errors;

    Base(cc_application.requests.Base request) throws JsonParseException {
        setup(request);
    }

    public void setup(cc_application.requests.Base request) throws JsonParseException {
        this.request = request;
        jsonResponse = new cc_application.jsample.Base("{}");
    }

    /**
     * Prepare, run, and build response for the use case
     * @return A Response Base derivative
     */
    public cc_application.responses.Base execute() throws InvalidRequestException {
        prepare();
        run();
        jsonResponse = buildResponse();
        return new cc_application.responses.Base(jsonResponse);
    }

    /**
     * Build the JSON-api conform response
     * @see <a href=http://jsonapi.org/format/#document-structure>JSON API document-structure (Top Level)</a>
     * @return A Response object
     */
    private cc_application.jsample.Base buildResponse() {
        saveMerge("meta", meta);
        saveMerge("data", data);
        saveAppend("errors", errors);
        return jsonResponse;
    }

    private void saveAppend(String key,  JSONAPIErrors errors) {
        if (errors != null)
            saveMerge(key, errors.errors());
    }

    private void saveMerge(String key, Object data) {
        if (data != null ) jsonResponse.update(key, data);
    }

    void addError(int status, String title) {
        if (errors() == null)
            setErrors(new JSONAPIErrors());
        errors().add(status,title);
        setErrors(errors());
    }

    private void prepare() {
        setData(null);
        setMeta(null);
        setErrors(null);
    }

    public HashMap meta() { return meta; }

    public final JSONAPIErrors errors() {
        return errors;
    }

    public final HashMap jsonApi() {
        return null;
    }

    public final HashMap links() {
        return null;
    }

    public final HashMap included() {
        return null;
    }

    public final Object data() {
        return data;
    }

    void setData(Object data) {
        this.data = data;
    }

    private void setMeta(HashMap meta) {
        this.meta = meta;
    }

    private void setErrors(JSONAPIErrors errors) {
        this.errors = errors;
    }

    protected abstract void run() throws InvalidRequestException;

}

package cc_application.jsonapi;

import cc_application.support.Config;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequest {
    private final String host;
    private final int port;
    private final String path;
    private final String method;
    private final String body;
    public StringBuffer response;
    private HttpURLConnection connection = null;

    public HttpRequest(String host, int port, String path) {
        this.host = host;
        this.port = port;
        this.path = path;
        this.method = "GET";
        this.body = "";
    }

    public void send() {
        try {
            preparedConnection();
            rawSendViaPreparedConnection(connection);
            fetchResponse(connection);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
    }

    private void fetchResponse(HttpURLConnection connection) throws IOException {
        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        response = new StringBuffer();
        String line;
        while((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\r');
        }
        rd.close();
    }

    private void rawSendViaPreparedConnection(HttpURLConnection connection) throws IOException {
        DataOutputStream wr = new DataOutputStream ( connection.getOutputStream());
        String urlParameters = "";
        wr.writeBytes(urlParameters);
        wr.close();
    }

    private HttpURLConnection preparedConnection() throws IOException {
        if (!(connection == null)) return connection;
        URL url = new URL(buildTargetURL());
        connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod(method.toUpperCase());
        connection.setRequestProperty("Content-type", Config.contentType("json"));
        connection.setRequestProperty("Content-Length", Integer.toString(body.getBytes().length));
        connection.setRequestProperty("Accept", Config.contentType("json"));
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        return connection;
    }

    private String buildTargetURL() {
        return String.format("http://%s:%d%s",host, port, path);
    }

    public String header(String key) throws IOException {
        connection = preparedConnection();
        String ct = connection.getRequestProperty(key);
        return ct == null ? "text/plain" : ct;
    }

    public String responseHeader(String key) {
        return String.valueOf(connection.getHeaderFields().get(key).get(0));
    }
}


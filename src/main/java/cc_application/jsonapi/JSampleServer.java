package cc_application.jsonapi;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;
import cc_application.exceptions.ServerStoppedByRequest;
import cc_application.requests.Base;
import cc_application.support.Config;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.logging.Level;

import static cc_application.support.json.jsonStringFromMap;
import static cc_application.usecases.Factory.buildUseCase;
import static java.lang.String.*;

public class JSampleServer {
    private final HttpServer httpServer;
    private boolean running;

    public JSampleServer(String host, int port) throws IOException {
        InetSocketAddress inetAddress = new InetSocketAddress(host, port);
        this.httpServer = HttpServer.create(inetAddress,Config.queueSize());
        this.running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public void start() {
        if (isRunning()) return;

        httpServer.start();
        this.running = true;
        httpServer.createContext("/", new JSampleHandler());

    }

    private void stop() throws ServerStoppedByRequest {
        stopQuietly();
        throw new ServerStoppedByRequest("Server stopped by request");
    }

    public void stopQuietly() {
        httpServer.stop(0);
        this.running = false;
    }

    public void stopSafeAndQuiet() {
        stopQuietly();
    }

    /**
     * <h1>HANDLE REQUESTS</h1>
     *
     *   - Input through httpExchange param
     *   - Build Request
     *   - Build Usecase
     *   - Run Usecase (returns a ResponseString)
     *   - Send Response to httpExchange
     *   - handle/shutdown if the request was a shutdown request
     */
    private class JSampleHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            cc_application.usecases.Base usecase;

            Base request = buildRequest(httpExchange);
            usecase = buildUsecase(request);
            String responseString = runUsecase(usecase);
            sendResponse(httpExchange, responseString);

            handleShutdownRequest(request);
        }

        private void sendResponse(HttpExchange httpExchange, String responseString) throws IOException {
            Headers responseHeaders = httpExchange.getResponseHeaders();
            responseHeaders.set("Content-type", Config.contentType("json"));
            httpExchange.sendResponseHeaders(200, responseString.length());
            OutputStream responseBody = httpExchange.getResponseBody();
            responseBody.write(responseString.getBytes());
            responseBody.close();
        }

        private String runUsecase(cc_application.usecases.Base usecase) {
            String responseString;
            cc_application.responses.Base jsonResponse;
            try {
                jsonResponse = usecase.execute();
                responseString = jsonResponse.toJson();
            } catch (InvalidRequestException e) {
                responseString = "<<ERROR RUNNING USECASE>>" + e.getMessage();
                e.printStackTrace();
            }
            return responseString;
        }

        private cc_application.usecases.Base buildUsecase(Base request) {
            cc_application.usecases.Base usecase = null;
            try {
                usecase = buildUseCase(request);
            } catch (JsonParseException | InvalidRequestException e) {
                e.printStackTrace();
            }
            return usecase;
        }

        private Base buildRequest(HttpExchange httpExchange) {
            Base request = null;
            try {
                request = buildRequestFromHttpExchange(httpExchange);
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            }
            return request;
        }

        private Base buildRequestFromHttpExchange(HttpExchange httpExchange) throws InvalidRequestException {
            String s = buildRequestJsonString(httpExchange);
            return new Base(s);
        }

        private String buildRequestJsonString(HttpExchange httpExchange) {
            HashMap<String,Object> requestMap = new HashMap<>();

            // build the request map
            requestMap.put("method", httpExchange.getRequestMethod());
            requestMap.put("path",   httpExchange.getRequestURI().getPath());
            setupBody(httpExchange, requestMap);

            // return the json string
            return jsonStringFromMap(requestMap);
        }

        private void setupBody(HttpExchange httpExchange, HashMap<String, Object> requestMap) {
            try {
                int lengthRead = httpExchange.getRequestBody().read();
                if( lengthRead > 0)
                        requestMap.put("body", httpExchange.getRequestBody().read());
                else
                    requestMap.remove("body");
            } catch (IOException e) {
                requestMap.remove("body");
                e.printStackTrace();
            }
        }
    }

    // Shutdown

    private void handleShutdownRequest(Base request) {
        try {
            if(isShutdownRequest(request)) {
                try {
                    stop();
                } catch (ServerStoppedByRequest serverStoppedByRequest) {
                    Config.log(Level.INFO, format("Terminated: %s", serverStoppedByRequest.getMessage()));
                }
            }
        } catch (InvalidRequestException e) {
            e.printStackTrace();
        }
    }

    private boolean isShutdownRequest(Base request) throws InvalidRequestException {
        return "server".equals(request.arguments().get(0)) && "shutdown".equals(request.arguments().get(1));
    }
}

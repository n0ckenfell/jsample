package cc_application.jsonapi;

/**
 * Dealing with the meta-part of a JSON-api response.
 * @see <a href="http://jsonapi.org/format/#document-meta">jsonapi document-meta</a>
 */
public class Meta {

    private final Object meta;

    public Meta(Object meta) {
        this.meta = meta;
    }
}

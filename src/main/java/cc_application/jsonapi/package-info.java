/**
 * The API to support the JSON-API protocol.
 * @see <a href="http://jsonapi.org">http://jsonapi.org</a>

 */
package cc_application.jsonapi;
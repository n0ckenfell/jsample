package cc_application.jsonapi;

/**
 * Dealing with JSON-API Errors
 * @see <a href="http://jsonapi.org/format/#errors">jsonapi.org - errors</a>
 */
public class Error {
    private final String status;
    private final String title;

    public Error(int status, String title) {
        this.status = String.format("%d", status);
        this.title  = title;
    }

    public String toJson() {
        return String.format("{\"id\":null, \"status\":\"%s\", \"title\":\"%s\"}", status(), title());
    }

    public String toString() {
        return String.format("JSON-API ERROR %s: %s", status(), title());
    }
    private String status() { return this.status; }
    private String title() {
        return this.title;
    }
}

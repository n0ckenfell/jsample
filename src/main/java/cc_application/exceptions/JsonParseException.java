package cc_application.exceptions;

/**
 * Thrown when ever the underlying json-library throws an Parse-Exception.
 */
public class JsonParseException extends Exception {
    public JsonParseException(String message) {
        super(message);
    }
}

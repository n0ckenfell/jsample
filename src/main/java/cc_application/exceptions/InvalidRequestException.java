package cc_application.exceptions;

/**
 * Thrown when a JSON Request string is invalid and Usecase can't be built
 * @see cc_application.usecases.Factory
 */
public class InvalidRequestException extends Exception {
    public InvalidRequestException(String message) {
        super(message);
    }
}

package cc_application.routes;

import cc_application.exceptions.InvalidRequestException;
import cc_application.exceptions.JsonParseException;
import cc_application.requests.Base;
import cc_application.usecases.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * A route can create a usecase from a request
 */
public class Route {
    private final cc_application.usecases.Base usecase;

    private Route(cc_application.usecases.Base usecase) {
        this.usecase = usecase;
    }

    public static Route buildRoute(Base request) throws InvalidRequestException, JsonParseException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<?> usecaseKlass = findUsecaseClass(request.useCaseName());
        Constructor<?> constructor = usecaseKlass.getConstructor(Base.class);

        cc_application.usecases.Base usecase = (cc_application.usecases.Base) constructor.newInstance(request);
        usecase.setup(request);
        return new Route(usecase);
    }

    private static Class<?> findUsecaseClass(String className) {
        String fqn = String.format("cc_application.usecases.%s",className);
        try {
            return Class.forName(fqn);
        } catch (ClassNotFoundException | NoClassDefFoundError e) {
            return Null.class;
        }
    }

    public cc_application.usecases.Base usecase() {
        return this.usecase;
    }
}


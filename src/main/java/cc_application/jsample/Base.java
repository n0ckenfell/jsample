package cc_application.jsample;

import cc_application.support.Config;
import cc_application.exceptions.JsonParseException;
import com.google.gson.Gson;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.logging.Level;

/**
 * API to access any 3rd party json-library.
 * <p>
 *   Base provides an API to objects, defined by JSON-strings
 *   regardless if it is an array or hash. <br>
 *   Obviously, calling map-methods on list-objects causes exceptions.
 * </p>
 */
@SuppressWarnings("unchecked")
public class Base implements Interface {
    private final String jsonString;
    private Object object;

    private static final JSONParser parser=new JSONParser();

    /**
     * Construct a Base object by parsing the given `jsonString`.
     * @param jsonString a valid json-string
     * @throws JsonParseException when the 3rd party library can not parse the string
     */
    public Base(String jsonString) throws JsonParseException {
        this.jsonString = jsonString;
        parse();
    }

    // Public API

    /**
     * @return The initial json-string used to initialize the object.
     */
    @Override
    public String initialJson() { return jsonString; }

    /**
     * @return The json-serialized current state of the object
     */
    @Override
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    // Map-methods

    /**
     * If `object` is a Map
     * @param key the key to search in the map
     * @return null or the object found.
     */
    @Override
    public Object getKey(Object key) {
        return ((JSONObject) object).get(key);
    }

    /**
     * If `object` is a Map
     * @param jsonString parse this string and merge the JSONObject with current state
     * @throws JsonParseException when `jsonString` can't be parsed by the 3rd party library
     */
    @Override
    public void merge(String jsonString) throws JsonParseException {
        JSONObject other = safeJsonObject(jsonString);
        ((JSONObject) object).putAll(other);
    }

    /**
     * Find the object identified by key and set the new value. If the key can't be found
     * a new entry will be added to the Map.
     * @param key the key to look up for
     * @param value the value found or null
     */
    @Override
    public void update(Object key, Object value) {
        ((JSONObject) object).put(key, value);
    }

    /**
     * Removes the mapping for the specified key from this map if present.
     * Does nothing if key doesn't exist.
     * @param key the key of the object to be removed
     */
    @Override
    public void removeKey(Object key) {
        ((JSONObject) object).remove(key);
    }


    // List-methods

    public int count() {
        return ((JSONArray) object).size();
    }
    /**
     * When the `object` is an Array/List.
     * @param idx 0-based index of the object
     * @return Any object found.
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public Object at(int idx) {
        return ((JSONArray) object).get(idx);
    }

    // Private

    private void parse() throws JsonParseException {
        try {
            object = parser.parse(jsonString);
        }  catch (ParseException e) {
            Config.log(Level.WARNING, "Error in JSON String " + jsonString);
            throw new JsonParseException(e.getMessage());
        }
    }

    private JSONObject safeJsonObject(String jsonString) throws JsonParseException {
        JSONObject other;
        try {
            other = (JSONObject) parser.parse(jsonString);
        } catch (ParseException e) {
            Config.log(
                    Level.WARNING, "Error in JSON String " + jsonString );
            throw new JsonParseException(e.getMessage());
        }
        return other;
    }




}

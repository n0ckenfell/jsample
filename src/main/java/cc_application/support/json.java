package cc_application.support;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Some basic Json handling
 */
public class json {
    public static String jsonStringFromMap(HashMap<String, Object> requestMap) {
        Gson gson = new Gson();
        return gson.toJson(requestMap, Map.class);
    }

}

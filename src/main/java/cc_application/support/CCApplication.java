package cc_application.support;

import cc_application.jsonapi.JSampleServer;

import java.io.IOException;

/**
 * Start the server
 */
class CCApplication {
    public static void main(String[] args)
    {
        JSampleServer server;

        try {
            server = new JSampleServer("localhost", Config.serverPort());
            server.start();
            System.out.println("Server started. Visit http://localhost:" + Config.serverPort());
        } catch (IOException e) {
            System.out.printf("Can't start server: %s%n", e.getMessage());
            e.printStackTrace();
        }
    }
}

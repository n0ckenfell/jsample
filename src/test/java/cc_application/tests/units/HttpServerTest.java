package cc_application.tests.units;

import cc_application.jsonapi.JSampleServer;
import cc_application.support.Config;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class HttpServerTest {
    private JSampleServer server;

    @Before
    public void setup() throws IOException, InterruptedException {
        server = new JSampleServer("localhost", Config.serverPort());
        server.start();
        assertTrue("Server should run", server.isRunning());
    }

    @After
    public void teardown() {
        server.stopQuietly();
        assertFalse("Server should be stopped", server.isRunning());
    }

    @Test
    public void sendingAPingRequest() {
        cc_application.jsonapi.HttpRequest httpRequest = new cc_application.jsonapi.HttpRequest("localhost", Config.serverPort(), "/ping");
        httpRequest.send();
        assertEquals("{\"data\":\"PONG\"}", httpRequest.response.toString().trim() );
    }

    @Test
    public void sendingAnEchoRequest() {
        cc_application.jsonapi.HttpRequest httpRequest = new cc_application.jsonapi.HttpRequest("localhost", Config.serverPort(), "/echo/hello");
        httpRequest.send();
        assertEquals("{\"data\":\"HELLO\"}", httpRequest.response.toString().trim() );
    }

    @Test
    public void shutdownRequest() {
        cc_application.jsonapi.HttpRequest httpRequest = new cc_application.jsonapi.HttpRequest("localhost", Config.serverPort(), "/server/shutdown");
        httpRequest.send();
        assertFalse("Server should be stopped after shutdown request but is running", server.isRunning());
    }

    @Test
    public void requestHeaderIsJsonAPI() throws IOException {
        cc_application.jsonapi.HttpRequest httpRequest = new cc_application.jsonapi.HttpRequest("localhost", Config.serverPort(), "/server/shutdown");
        assertEquals(Config.contentType("json"), httpRequest.header("Content-type"));
    }

    @Test
    public void responseHeaderIsJsonAPI() {
        cc_application.jsonapi.HttpRequest httpRequest = new cc_application.jsonapi.HttpRequest("localhost", Config.serverPort(), "/server/shutdown");
        httpRequest.send();
        assertEquals(Config.contentType("json"), httpRequest.responseHeader("Content-type"));
    }
}

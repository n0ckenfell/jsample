package cc_application.tests.usecases;

import cc_application.exceptions.InvalidRequestException;
import cc_application.responses.Base;
import cc_application.usecases.Factory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.time.ZonedDateTime;
import java.util.HashMap;

import static org.junit.Assert.*;

@SuppressWarnings("unused")
public class Steps {

    private cc_application.requests.Base request = null;
    private Base response = null;
    private String requestJson;
    private ZonedDateTime currentDate;

    @Given("The request (.*)$")
    public void TheRequest(String jsonString) throws Throwable {
        request = new cc_application.requests.Base(jsonString);
    }

    @When("^request\\.isValid$")
    public void requestIsValidIsTrue() throws Throwable {
        assertTrue(request.isValid());
    }

    @And("^Request\\.path is (.*)$")
    public void requestPathIsPing(String path) throws Throwable {
        assertEquals( path, request.path());
    }

    @And("^Request\\.method is (.*)$")
    public void requestMethodIsGet(String method) throws Throwable {
        assertEquals( method, request.method());
    }

    @And("^Request\\.body is empty$")
    public void requestBodyIsEmpty() throws Throwable {
        assertBlank(request.body());
    }

    private void assertBlank(String str) {
       assertEquals("", str);
    }

    @When("^executing the request$")
    public void executingTheRequest() throws Throwable {
        cc_application.usecases.Base useCase = Factory.buildUseCase(request);
        response = useCase != null ? useCase.execute() : null;
    }

    @Then("^the response body is (.*)$")
    public void responseBodyIs(String responseBody) throws Throwable {
        assertEquals(responseBody, response.body());
    }

    @Given("^an invalid json-string$")
    public void anInvalidJsonString() throws Throwable {
        requestJson = "{\"no_path\" : \"is_invalid\", \"method\" : \"get\"}";
    }

    @Then("^expect an InvalidRequestException to be thrown$")
    public void expectAnInvalidRequestExceptionToBeThrown() throws Throwable {
        try {
            cc_application.requests.Base request = new cc_application.requests.Base(requestJson);
            Factory.buildUseCase(request);
            fail("Usecase should be invalid for " + requestJson + " but no exception was thrown.");
        } catch (InvalidRequestException e) {
            // Noop
        }
    }

    @Given("^the string (.*)$")
    public void theString(String str) throws Throwable {
        requestJson = str;
    }


    @Then("^the request defaults to method get$")
    public void theRequestDefaultsToMethodGet() throws Throwable {
        cc_application.requests.Base request = new cc_application.requests.Base(requestJson);
        assertEquals(request.method(), "GET");
    }

    @Given("^An empty request to \"([^\"]*)\"$")
    public void anEmptyRequestTo(String path) throws Throwable {
        requestJson = String.format("{ \"path\":\"%s\"}", path);
        request = new cc_application.requests.Base(requestJson);
    }

    @Then("^The response has an empty meta map$")
    public void theResponseHasAnEmptyMetaMap() throws Throwable {
        assertTrue(response.hasMeta());
    }

    @And("^The response has an empty data map$")
    public void theResponseHasAnEmptyDataMap() throws Throwable {
        assertEquals(response.data(), new HashMap());
    }

    @And("^The response has no errors$")
    public void theResponseHasNoErrors() throws Throwable {
        assertNull(response.errors());
    }

    @Given("^a Fail usecase request$")
    public void aFailingUsecaseRequest() throws Throwable {
        request = new cc_application.requests.Base("{\"path\":\"fail\"}");
        cc_application.usecases.Base useCase = Factory.buildUseCase(request);
        response = useCase != null ? useCase.execute() : null;
    }

    @Then("^The response has no data map$")
    public void theResponseHasNoDataMap() throws Throwable {
        assertNull(response.data());
    }

    @Then("^The response is (.*)$")
    public void theResponseIs(String expected) throws Throwable {
        assertEquals(expected, response.toJson());
    }

    @Given("^The expected date is the current date")
    public void theExpectedYearOfCurrentDate() throws Throwable {
        currentDate = ZonedDateTime.now();
    }

    @Then("^response is the current date$")
    public void responseIsTheCurrentDate() throws Throwable {
        HashMap map = (HashMap) response.data();
        assertEquals(map.get("year"), currentDate.getYear());
        assertEquals(map.get("month"), currentDate.getMonthValue());
        assertEquals(map.get("dayOfMonth"), currentDate.getDayOfMonth());
    }

    @And("^The expected date is \"([^\"]*)\"$")
    public void theExpectedDateIs(String givenDate) throws Throwable {
        currentDate = ZonedDateTime.parse(givenDate);
    }

    @Then("^response is Julian's birthday$")
    public void responseIsJulianSBirthday() throws Throwable {
        HashMap map = (HashMap) response.data();
        ZonedDateTime julian = ZonedDateTime.parse("1995-02-17T22:28:00+01:00");
        assertEquals(julian.getYear(), map.get("year"));
    }

    @Then("^response includes error \"([^\"]*)\"$")
    public void responseIncludesError(String errorMessage) throws Throwable {
        cc_application.jsonapi.Error error = (cc_application.jsonapi.Error) response.errors().get(0);
        assertEquals(error.toString(), errorMessage);
    }

    @Then("^the response doesn't include data$")
    public void theResponseDoesnTIncludeData() throws Throwable {
        Object data = response.data();
        assertNull(data);
    }
}


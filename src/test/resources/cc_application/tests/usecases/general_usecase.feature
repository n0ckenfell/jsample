Feature: General Usecase

  Scenario: A Request has a method, a path, and a body
    Given The request { "method" : "get", "path" : "ping" }
    When request.isValid
    Then Request.path is ping
    And Request.method is get
    And Request.body is empty

  Scenario: The ping request answers pong
    Given The request { "method" : "get", "path" : "ping" }
    When executing the request
    Then the response body is {"data":"PONG"}

  Scenario: An invalid json string throws an InvalidRequestException
    Given an invalid json-string
    Then expect an InvalidRequestException to be thrown

  Scenario: A Request without a method defaults to get
    Given the string { "path" : "/" }
    Then the request defaults to method get

  Scenario Outline: Testing simple requests with defaults
    Given The request <jsonString>
    When executing the request
    Then the response body is <responseBody>
    Examples:
      | jsonString                                              | responseBody               |
      | { "path" : "unknown path leads to default NullUsecase"} | {"data":"NULL"}  |
      | { "method" : "get", "path" : "ping"                   } | {"data":"PONG"}  |
      | { "method" : "get", "path" : "echo", "body" : "Hello" } | {"data":"HELLO"} |
      | { "method" : "get", "path" : "echo", "body" : "World" } | {"data":"WORLD"} |
      | { "method" : "get", "path" : "echo"                   } | {"data":""}      |
    
  Scenario: Time API - current time
    Given The request { "path" : "/time" }
    And The expected date is the current date
    When executing the request
    Then response is the current date

  Scenario: Time API - a specific time
    Given The request { "path" : "/time/1995-02-17T22:28:00+01:00" }
    And The expected date is "1995-02-17T22:28:00+01:00"
    When executing the request
    Then response is Julian's birthday

  Scenario: Time API - time with errors
    Given The request { "path" : "/time/1995-13-99T99:99:99+99:99" }
    When executing the request
    Then the response doesn't include data
    And response includes error "JSON-API ERROR 0: Text '1995-13-99T99:99:99+99:99' could not be parsed at index 19"

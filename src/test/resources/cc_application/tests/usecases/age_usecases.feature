Feature: Time Age

  Scenario: 1 year has 365 days unless it is a leep year
    When executing the request { "method" : "get", "path" : "age/2003-01-01/2004-01-01" }
    Then the responded age is 365 days

  Scenario: 1 year in a leap year has 366 days
    When executing the request { "method" : "get", "path" : "age/2000-01-01/2001-01-01" }
    Then the responded age is 366 days

  Scenario: 1 year back in a leap year is -366 days
    When executing the request { "method" : "get", "path" : "age/2001-01-01/2000-01-01" }
    Then the responded age is -366 days

  Scenario: Days back from Andi's to Heidi's date of birth
    When executing the request { "method" : "get", "path" : "age/1969-08-28/1964-08-31" }
    Then the responded age is -1823 days

  Scenario Outline: Testing several age requests
    Given The request <jsonString>
    When executing the request
    Then the response body is <responseBody>
    Examples:
      | jsonString                                                  | responseBody             |
      | { "method" : "get", "path" : "/age/2016-01-01/1964-08-31" } | {"data":{"days":-18750}} |
      | { "method" : "get", "path" : "/age/1964-08-31/1969-08-28" } | {"data":{"days":1823}}   |
      | { "method" : "get", "path" : "/age/2002-01-01/2001-01-01" } | {"data":{"days":-365}}   |
      | { "method" : "get", "path" : "/age/2000-01-01/2001-01-01" } | {"data":{"days":366}}    |

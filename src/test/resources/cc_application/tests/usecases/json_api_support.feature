Feature: JSON API Support

  Scenario: A minimum valid response
    Given An empty request to "a path ending up in a Null use case"
    When executing the request
    Then The response has an empty meta map
    And The response is {"data":"NULL"}
    And The response has no errors

  Scenario: A minimum failed use case responds no data but errors
    Given a Fail usecase request
    When executing the request
    Then The response has no data map
    Then The response is {"errors":[{"status":"400","title":"Test friendly failed on request"}]}

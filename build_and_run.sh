#!/bin/bash

if [ "$1" == "-r" ]
then
  echo "Skip build ..."
else
  # build with bundled artifacts
  mvn clean package
fi

# run the server
java -cp target/cc_application-0.0.1-jar-with-dependencies.jar cc_application.support.CCApplication
